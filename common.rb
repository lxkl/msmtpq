require "digest"
require "fileutils"
require "open3"
require "time"
require "uuid"

def get_dir
  ENV["MSMTPQ_DIR"] || File.expand_path("~/.msmtp.queue")
end

def obtain_lock(dir, me)
  ["temp", "curr", "sent", "fail"].each { |x| FileUtils.mkdir_p("#{dir}/#{x}") }
  lockfile = File.open("#{dir}/lock.#{me}", File::CREAT)
  unless lockfile.flock(File::LOCK_EX | File::LOCK_NB)
    puts "msmtpq-#{me}: fatal: cannot obtain lock"
    exit 111
  end
end
