#!/usr/bin/env ruby
require_relative "common"

## Code written by Lasse Kliemann <lasse@lassekliemann.de>
## in June and July 2020.
## See https://gitlab.com/lxkl/msmtpq for the latest version
## and for documentation.

def main
  warn_maildir = ENV["MSMTPQ_WARN_MAILDIR"]
  warn_intervals = ENV["MSMTPQ_WARN_INTERVALS"] || "60,1800,3600,14400"
  warn_fail = ENV["MSMTPQ_WARN_FAIL"] || "10"
  dir = get_dir
  obtain_lock(dir, "flush")
  Dir.glob("#{dir}/curr/*").each do |curr|
    next unless File.directory?(curr)
    is_complete = true
    ["args", "text", "stamp-0000"].each do |base|
      fn = "#{curr}/#{base}"
      unless File.exist?(fn)
        puts "msmtpq-flush: warning: missing file: #{fn}"
        is_complete = false
      end
    end
    next unless is_complete
    puts "msmtpq-flush: processing: #{curr}"
    args = File.readlines("#{curr}/args").map(&:chomp)
    message_text = File.read("#{curr}/text")
    msmtp_out, msmtp_status = Open3.capture2e(*(["msmtp"] + args), stdin_data: message_text)
    puts msmtp_out
    if msmtp_status.success?
      FileUtils.mv(curr, "#{dir}/sent")
    else
      puts "msmtpq-flush: warning: unable to send: #{curr}"
      if warn_maildir
        warn_maildir = File.expand_path(warn_maildir)
        ["tmp", "new", "cur"].each { |x| FileUtils.mkdir_p("#{warn_maildir}/#{x}") }
        stamp_list = Dir.glob("#{curr}/stamp-*").sort
        warn_interval_list = warn_intervals.split(",").map(&:to_i)
        interval = warn_interval_list[[warn_interval_list.length, stamp_list.length].min - 1]
        next_number_fmt = "%04d" % stamp_list.length
        now = Time.now
        if now - File.mtime(stamp_list.last) > interval
          is_fail = false
          if stamp_list.length < warn_fail.to_i
            warn_subject = "warning"
            warn_text = "This is a warning only. I will keep trying to send the message."
          else
            warn_subject = "fail"
            warn_text = "The message is now being moved to the 'fail' directory. I will no longer try to send it."
            is_fail = true
          end
          time_since_start = now - File.mtime("#{curr}/stamp-0000")
          time_since_start_in_hours = (time_since_start / 3600.0).round(1)
          mail_file = "#{warn_maildir}/tmp/" + File.basename(curr) + "." + next_number_fmt
          File.write(mail_file, <<_EOT_)
From: msmtpq-flush <msmtpq-flush@invalid.invalid>
Subject: msmtpq-flush: #{warn_subject}: #{File.basename(curr)}
Date: #{now.strftime("%a, %-d %b %Y %T %z")}

Hi. This is the msmtpq-flush program. I was unable to send the message in

#{curr}

for #{time_since_start.round} seconds (about #{time_since_start_in_hours} hours).

#{warn_text}

The output of the msmtp program was:

#{msmtp_out}
The first 50 lines of the message are:

#{message_text.split("\n")[0..49].join("\n")}
_EOT_
          FileUtils.mv(mail_file, "#{warn_maildir}/new/" + File.basename(mail_file) + ":2,")
          if is_fail
            FileUtils.mv(curr, "#{dir}/fail")
          else
            FileUtils.touch("#{curr}/stamp-#{next_number_fmt}")
          end
        end
      end
    end
  end
end

main
